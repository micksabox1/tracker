## Tracker DEV Mysql setup

This project expects a mysql server to be running locally.  Use the folowing command to start a mysql server:

```
docker run -d --name skyhub-mysql \
    -p 3306:3306 \
    --env MYSQL_DATABASE=skyhub_tracker \
    --env MYSQL_USER=skyhub \
    --env MYSQL_PASSWORD=3fSS34f03lls \
    --env MYSQL_ROOT_PASSWORD=byMp3nTXpaKeB7Vz \
    --env MYSQL_ROOT_HOST=% \
    mariadb:10.1
```    

If you are running trackerd locally, you can also run this command to create the skyhub-trackerd database;

`docker exec -it skyhub-mysql mysql -u root -pbyMp3nTXpaKeB7Vz -e 'CREATE DATABASE skyhub_trackerd;'`

Be sure to wait for the container to fully  start before you run the skyhub-trackerd database creation command.
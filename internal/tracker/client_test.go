package tracker

import (
	"context"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"log"
	"os"
	"testing"
	"time"
)

func TestConnectionSetup(t *testing.T) {
	_, err := NewClientGRPC(ClientGRPCConfig{
		Address: "",
	})

	if err == nil {
		t.Errorf("Should fail for empty address")
	}

}

func TestGrpcConnectionRetry(t *testing.T) {

	envTrackerdAddr := os.Getenv("trackerd-server-addr")

	if envTrackerdAddr == "" {
		return
	}

	client, err := NewClientGRPC(ClientGRPCConfig{
		Address: envTrackerdAddr,
	})

	if err != nil {
		t.Errorf("Could not connect to trackerd grpc")
	}

	info := &pb.TrackerInfo{Uuid: "UUID", Name: "Test"}

	event := &pb.Event{
		Tracker: info,
		Uuid:    "Test1",
		Source:  &pb.EventSource{},
	}

	response, err := client.Client.AddEvent(context.Background(), event)

	if err != nil {
		t.Errorf("Register gRPC call failed %s", err)
	} else {
		log.Printf("AddEvent response %s", response.String())
	}

	log.Println("Sleeping for 10 seconds, pause/stop trackerd instance before timer runs out")
	time.Sleep(10 * time.Second)
	log.Println("Waking up, trackerd should be stopped now...")

	event.Uuid = "Test2"

	log.Println("Attempting to call gRPC method. Once you've resumed/restarted trackerd, test should pass successfully")
	response, err = client.Client.AddEvent(context.Background(), event)

	if err != nil {
		t.Errorf("Register gRPC call failed %s", err)
	} else {
		log.Printf("AddEvent response 2 %s", response.String())
	}
}

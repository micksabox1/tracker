/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package db

import (
	"fmt"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
)

type Model struct {
	ID          uint `gorm:"primary_key"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
	TrackerId   string
	TrackerName string
	EventId     string
	GpsTime     time.Time
	LatLong     string
}

type TrackerInfo struct {
	Model
	pb.TrackerInfo
}

type Event struct {
	Model
	pb.Event
}

type VideoEvent struct {
	Model
	pb.VideoEvent
}

type SensorReport struct {
	Model
	pb.SensorReport
}

type GPS_TPVReport struct {
	Model
	pb.GPS_TPVReport
}

type GPS_SKYReport struct {
	Model
	pb.GPS_SKYReport
}

type GPS_GSTReport struct {
	Model
	pb.GPS_GSTReport
}

type GPS_ATTReport struct {
	Model
	pb.GPS_ATTReport
}

type GPS_VERSIONReport struct {
	Model
	pb.GPS_VERSIONReport
}

type GPS_DEVICESReport struct {
	Model
	pb.GPS_DEVICESReport
}

type GPS_DEVICEReport struct {
	Model
	pb.GPS_DEVICEReport
}

type GPS_PPSReport struct {
	Model
	pb.GPS_PPSReport
}

type GPS_ERRORReport struct {
	Model
	pb.GPS_ERRORReport
}

type GPS_Satellite struct {
	Model
	pb.GPS_Satellite
}

type Time struct {
	Model
	pb.Time
}

type Angle struct {
	Model
	pb.Angle
}

type Accelerometer struct {
	Model
	pb.Accelerometer
}

type Gyroscope struct {
	Model
	pb.Gyroscope
}

type Magnetometer struct {
	Model
	pb.Magnetometer
}

type Pressure struct {
	Model
	pb.Pressure
}

type Altitude struct {
	Model
	pb.Altitude
}

type LonLat struct {
	Model
	pb.LonLat
}

type DB struct {
	Handle *gorm.DB
	uri    string
}

func NewDB() *DB {
	return &DB{}
}

func (db *DB) Connect(uri string) (err error) {
	db.uri = uri

	var nAttemps int = 0
	var maxAttempts int = 180

	for {
		if nAttemps == 0 {
			log.Printf("Waiting for database to come online\n")
		} else if nAttemps > maxAttempts {
			log.Printf("Failed connecting to database after %d seconds\n", maxAttempts)
			return
		}
		log.Printf("Failed connecting to database after %d seconds\n", nAttemps)
		db.Handle, err = gorm.Open("mysql", db.uri)

		if err == nil {
			db.Handle.DB().SetConnMaxLifetime(time.Minute * 5)
			db.Handle.DB().SetMaxIdleConns(0)
			db.Handle.DB().SetMaxOpenConns(20)

			break
		}

		time.Sleep(1 * time.Second)
		nAttemps++
	}
	log.Printf("Successfully connected to mysql\n")

	db.Handle.AutoMigrate(
		&Event{},
		&VideoEvent{},
		&TrackerInfo{},
		&GPS_TPVReport{},
		&GPS_TPVReport{},
		&GPS_SKYReport{},
		&GPS_GSTReport{},
		&GPS_ATTReport{},
		&GPS_VERSIONReport{},
		&GPS_DEVICESReport{},
		&GPS_DEVICEReport{},
		&GPS_PPSReport{},
		&GPS_ERRORReport{},
		&GPS_Satellite{},
		&Time{},
		&Angle{},
		&Accelerometer{},
		&Gyroscope{},
		&Magnetometer{},
		&Pressure{},
		&Altitude{},
	)

	return
}

func (db *DB) saveSensorReport(in *pb.SensorReport) {
	if in.GPS_TPVReport == nil {
		return
	}

	model := Model{
		TrackerId:   in.Tracker.Uuid,
		TrackerName: in.Tracker.Name,
		GpsTime:     in.GPS_TPVReport.Time.AsTime().UTC(),
		LatLong:     fmt.Sprintf("%f,%f", in.GPS_TPVReport.Lat, in.GPS_TPVReport.Lon),
		EventId:     in.EventId,
	}

	if in.Accelerometer != nil {
		accel := Accelerometer{Model: model, Accelerometer: *in.Accelerometer}
		db.Handle.Save(&accel)
	}

	if in.Gyroscope != nil {
		gyro := Gyroscope{Model: model, Gyroscope: *in.Gyroscope}
		db.Handle.Save(&gyro)
	}

	if in.Angle != nil {
		angle := Angle{Model: model, Angle: *in.Angle}
		db.Handle.Save(&angle)
	}

	if in.Magnetometer != nil {
		magnet := Magnetometer{Model: model, Magnetometer: *in.Magnetometer}
		db.Handle.Save(&magnet)
	}

	if in.Pressure != nil {
		press := Pressure{Model: model, Pressure: *in.Pressure}
		db.Handle.Save(&press)
	}

	if in.Altitude != nil {
		alt := Altitude{Model: model, Altitude: *in.Altitude}
		db.Handle.Save(&alt)
	}

	if in.LonLat != nil {
		lonlat := LonLat{Model: model, LonLat: *in.LonLat}
		db.Handle.Save(&lonlat)
	}

	if in.GPS_TPVReport != nil {
		tpv := GPS_TPVReport{Model: model, GPS_TPVReport: *in.GPS_TPVReport}
		db.Handle.Save(&tpv)
	}

	if in.GPS_SKYReport != nil {
		sky := GPS_SKYReport{Model: model, GPS_SKYReport: *in.GPS_SKYReport}
		//sky.GPS_Satellite = in.GPS_SKYReport.GPS_Satellite
		db.Handle.Save(&sky)
	}

	if in.GPS_GSTReport != nil {
		gst := GPS_GSTReport{Model: model, GPS_GSTReport: *in.GPS_GSTReport}
		db.Handle.Save(&gst)
	}

	if in.GPS_ATTReport != nil {
		att := GPS_ATTReport{Model: model, GPS_ATTReport: *in.GPS_ATTReport}
		db.Handle.Save(&att)
	}

	if in.GPS_VERSIONReport != nil {
		version := GPS_VERSIONReport{Model: model, GPS_VERSIONReport: *in.GPS_VERSIONReport}
		db.Handle.Save(&version)
	}

	if in.GPS_DEVICESReport != nil {
		devices := GPS_DEVICESReport{Model: model, GPS_DEVICESReport: *in.GPS_DEVICESReport}
		//devices.GPS_DEVICEReport = in.GPS_DEVICEReport.GPS_DEVICEReport
		//devices.Remote = in.GPS_DEVICEReport.Remote
		db.Handle.Save(&devices)
	}

	if in.GPS_DEVICEReport != nil {
		device := GPS_DEVICEReport{Model: model, GPS_DEVICEReport: *in.GPS_DEVICEReport}
		db.Handle.Save(&device)
	}

	if in.GPS_PPSReport != nil {
		pps := GPS_PPSReport{Model: model, GPS_PPSReport: *in.GPS_PPSReport}
		db.Handle.Save(&pps)
	}

	if in.GPS_ERRORReport != nil {
		gps_error := GPS_ERRORReport{Model: model, GPS_ERRORReport: *in.GPS_ERRORReport}
		db.Handle.Save(&gps_error)
	}
}

func (db *DB) saveEvent(in *pb.Event) {
	if in == nil {
		return
	}

	model := Model{
		TrackerId:   in.Tracker.Uuid,
		TrackerName: in.Tracker.Name,
		GpsTime:     in.Tracker.Time.AsTime().UTC(),
		LatLong:     fmt.Sprintf("%f,%f", in.Tracker.Latitude, in.Tracker.Longitude),
		EventId:     in.Uuid,
	}

	if in != nil {
		event := Event{Model: model, Event: *in}
		db.Handle.Save(&event)
	}
}

func (db *DB) saveVideoEvent(in *pb.VideoEvent) {
	if in == nil {
		return
	}

	model := Model{
		TrackerId:   in.Tracker.Uuid,
		TrackerName: in.Tracker.Name,
		GpsTime:     in.Tracker.Time.AsTime().UTC(),
		LatLong:     fmt.Sprintf("%f,%f", in.Tracker.Latitude, in.Tracker.Longitude),
		EventId:     in.EventUuid,
	}

	if in != nil {
		videoEvent := VideoEvent{Model: model, VideoEvent: *in}
		db.Handle.Save(&videoEvent)
	}
}

func (db *DB) SetVideoReceived(eventId string, uri string) bool {
	var (
		rec VideoEvent
	)

	rec.EventUuid = eventId

	err := db.Handle.Model(&rec).Updates(pb.VideoEvent{VideoReceived: true, Uri: uri}).Error

	if err != nil {
		log.Printf("Update returned: %s\n", err)
		return false
	}

	return true
}

func (db *DB) SetThumbnailReceived(eventId string, uri string) bool {
	var (
		rec VideoEvent
	)

	rec.EventUuid = eventId

	err := db.Handle.Model(&rec).Updates(pb.VideoEvent{ThumbnailReceived: true, Thumbnail: uri}).Error

	if err != nil {
		log.Printf("Update returned: %s\n", err)
		return false
	}

	return true
}
func (db *DB) SetVideoSynced(eventId string, uri string) bool {
	var (
		rec VideoEvent
	)

	rec.EventUuid = eventId

	err := db.Handle.Model(&rec).Updates(pb.VideoEvent{VideoSynced: true, Uri: uri}).Error

	if err != nil {
		log.Printf("Update returned: %s\n", err)
		return false
	}

	return true
}

func (db *DB) SetThumbnailSynced(eventId string, uri string) bool {
	var (
		rec VideoEvent
	)

	rec.EventUuid = eventId

	err := db.Handle.Model(&rec).Updates(pb.VideoEvent{ThumbnailSynced: true, Thumbnail: uri}).Error

	if err != nil {
		log.Printf("Update returned: %s\n", err)
		return false
	}

	return true
}

func (db *DB) GetNextUnsyncedVideo() *VideoEvent {
	var (
		videoEvent VideoEvent
	)

	//r := db.Handle.Where("video_received = ? AND video_synced = ?", true, false).First(&videoEvent)
	r := db.Handle.First(&videoEvent, "video_received = ? AND video_synced = ?", 1, 0)

	if r.Error != nil || r.RowsAffected < 1 {
		return nil
	}

	return &videoEvent
}

func (db *DB) Save(o interface{}) {
	switch v := o.(type) {
	case *pb.SensorReport:
		db.saveSensorReport(v)
	case *pb.Event:
		db.saveEvent(v)
	case *pb.VideoEvent:
		db.saveVideoEvent(v)
	default:
		log.Printf("unknown type: %T", v)
	}
}

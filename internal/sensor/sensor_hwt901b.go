/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"bytes"
	"encoding/binary"
	"log"

	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"go.bug.st/serial"
)

type HWT901BSensor struct {
	data         []interface{}
	port         serial.Port
	pollInterval int
	DevicePath   string
}

type Time struct {
	Year        byte
	Month       byte
	Day         byte
	Hour        byte
	Minute      byte
	Second      byte
	Millisecond int16
}

type Acceleration struct {
	X int16
	Y int16
	Z int16
	T int16
}

type Gyroscope struct {
	X int16
	Y int16
	Z int16
	T int16
}

type Angle struct {
	X int16
	Y int16
	Z int16
	T int16
}

type AltitudePressure struct {
	Pressure int32
	Altitude int32
}

type Magnetometer struct {
	X int16
	Y int16
	Z int16
	T int16
}

type LonLat struct {
	Lon int
	Lat int
}

func (a Angle) Pack() *pb.Angle {
	x := int32(a.X) / 32768 * 180
	y := int32(a.Y) / 32768 * 180
	z := int32(a.Z) / 32768 * 180

	o := &pb.Angle{
		X:      x,
		Y:      y,
		Z:      z,
		Sensor: pb.Sensor_WITMOTION_HWT901B,
	}

	return o
}

func (g Gyroscope) Pack() *pb.Gyroscope {
	x := int32(g.X)
	y := int32(g.Y)
	z := int32(g.Z)

	o := &pb.Gyroscope{
		X:      x,
		Y:      y,
		Z:      z,
		Sensor: pb.Sensor_WITMOTION_HWT901B,
	}
	return o
}

func (x Time) Pack() *pb.Time {
	o := &pb.Time{
		Sensor:      pb.Sensor_WITMOTION_HWT901B,
		Year:        int32(x.Year),
		Month:       int32(x.Month),
		Day:         int32(x.Day),
		Hour:        int32(x.Hour),
		Minute:      int32(x.Minute),
		Second:      int32(x.Second),
		Millisecond: int32(x.Millisecond),
	}
	return o
}

func (a Acceleration) Pack() *pb.Accelerometer {
	x := float32(a.X) / 32768 * 16
	y := float32(a.Y) / 32768 * 16
	z := float32(a.Z) / 32768 * 16

	o := &pb.Accelerometer{
		Sensor: pb.Sensor_WITMOTION_HWT901B,
		X:      x,
		Y:      y,
		Z:      z,
	}
	return o
}
func (m Magnetometer) Pack() *pb.Magnetometer {
	x := int32(m.X)
	y := int32(m.Y)
	z := int32(m.Z)

	o := &pb.Magnetometer{
		Sensor: pb.Sensor_WITMOTION_HWT901B,
		X:      x,
		Y:      y,
		Z:      z,
	}

	return o
}

func (ap AltitudePressure) Pack() (*pb.Pressure, *pb.Altitude) {
	press := &pb.Pressure{
		Pressure: ap.Pressure,
	}

	alt := &pb.Altitude{
		Sensor:   pb.Sensor_WITMOTION_HWT901B,
		Altitude: ap.Altitude / 100,
	}

	return press, alt
}

func (ll LonLat) Pack() *pb.LonLat {
	o := &pb.LonLat{
		Sensor: pb.Sensor_WITMOTION_HWT901B,
		Lon:    float64(ll.Lon%10000000) / 1e5,
		Lat:    float64(ll.Lat%10000000) / 1e5,
	}

	return o
}

func (s *HWT901BSensor) name() string {
	return pb.Sensor_name[int32(pb.Sensor_WITMOTION_HWT901B)]
}

func (s *HWT901BSensor) openPort() (port serial.Port, err error) {
	var (
		mode *serial.Mode
	)

	mode = &serial.Mode{
		BaudRate: 9600,
		Parity:   serial.NoParity,
		StopBits: serial.OneStopBit,
	}

	port, err = serial.Open(s.DevicePath, mode)

	if err != nil {
		return
	}

	port.SetDTR(true)
	port.SetRTS(false)

	return
}

func readIntoStruct(buff []byte, out interface{}) {
	buffer := bytes.NewBuffer(buff)
	err := binary.Read(buffer, binary.LittleEndian, out)
	if err != nil {
		log.Fatal("binary.Read failed", err)
	}
}

func (s *HWT901BSensor) read() []interface{} {
	return s.data
}

func (s *HWT901BSensor) init() bool {
	var (
		err error
	)

	s.port, err = s.openPort()

	if err != nil {
		return false
	}

	go s.poll()

	return true
}

func (s *HWT901BSensor) close() bool {
	return true
}

func (s *HWT901BSensor) poll() {
	for {
		buff := make([]byte, 2000)
		n, err := s.port.Read(buff)
		if err != nil {
			log.Fatal(err)
			break
		}

		if n == 0 {
			break
		}

		for n >= 11 {
			if buff[0] != 0x55 {
				n--
				buff = buff[1:]
				continue
			}

			switch buff[1] {
			case 0x50:
				var time Time
				readIntoStruct(buff[2:10], &time)
				s.data = append(s.data, time.Pack())
			case 0x51:
				var acc Acceleration
				readIntoStruct(buff[2:10], &acc)
				s.data = append(s.data, acc.Pack())
			case 0x52:
				var gyro Gyroscope
				readIntoStruct(buff[2:10], &gyro)
				s.data = append(s.data, gyro.Pack())
			case 0x53:
				var angle Angle
				readIntoStruct(buff[2:10], &angle)
				s.data = append(s.data, angle.Pack())
			case 0x54:
				var mag Magnetometer
				readIntoStruct(buff[2:10], &mag)
				s.data = append(s.data, mag.Pack())
			case 0x56:
				var altpress AltitudePressure
				readIntoStruct(buff[2:10], &altpress)
				press, alt := altpress.Pack()
				s.data = append(s.data, press)
				s.data = append(s.data, alt)
			case 0x57:
				var lonlat LonLat
				readIntoStruct(buff[2:10], &lonlat)
				s.data = append(s.data, lonlat.Pack())
			}
			n -= 11
			buff = buff[11:]
		}
	}
}

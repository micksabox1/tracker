FROM golang:1.15.6-alpine as build-stage
RUN apk add --no-cache git build-base libc6-compat
ADD . /app/
WORKDIR /app
RUN make tracker-controller


FROM alpine:3.12 as production-stage
RUN apk add --no-cache libc6-compat
RUN mkdir -p /skyhub/db && \
    mkdir -p /skyhub/etc && \
    mkdir -p /skyhub/data && \
    mkdir /app

COPY --from=build-stage /app/cmd/bin/arm64/linux/tracker-controller /app/tracker-controller

CMD ["/app/tracker-controller"]
